FROM rust:1.81.0 AS build
WORKDIR /usr/src/tidyvcf
COPY . .
RUN cargo install --locked --path .


FROM debian:12
COPY --from=build /usr/local/cargo/bin/tidyvcf /usr/local/bin/tidyvcf
RUN tidyvcf --help
CMD ["tidyvcf"]
