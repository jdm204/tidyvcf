use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn file_doesnt_exist() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tidyvcf")?;

    cmd.arg("-i").arg("test/file/doesnt/exist");
    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("No such file"));

    Ok(())
}

#[test]
fn spec_noncompliance_allowed_lenient() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tidyvcf")?;

    cmd.arg("-i").arg("resources/test_nonspec.vcf").arg("-l");
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("chr1"));

    Ok(())
}

#[test]
fn matches_previous_run() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tidyvcf")?;

    cmd.arg("-v")
        .arg("-s")
        .arg("-i")
        .arg("resources/test.vcf")
        .arg("-o")
        .arg("test.tsv");
    cmd.assert().success();

    let expected = std::fs::read_to_string("resources/expected.tsv")?;
    let got = std::fs::read_to_string("test.tsv")?;
    assert!(expected == got);

    std::fs::remove_file("test.tsv")?;
    Ok(())
}

#[test]
fn subsetting_columns_works() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("tidyvcf")?;

    cmd.arg("-i")
        .arg("resources/test.vcf")
        .arg("--just")
        .arg(" pos")
        .arg("info_DP")
        .arg("-v");
    cmd.assert().success();
    Ok(())
}
