{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [
      bashInteractive
      rustc cargo rust-analyzer rustfmt pkg-config bcftools
    ];
}
