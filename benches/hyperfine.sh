#!/bin/bash

RELEASE_BIN="../target/release/tidyvcf"
TEST_VCF="../resources/test.vcf"
EXPECTED_TABLE="../resources/expected.tsv"
OUTPUT_TABLE="../resources/test.tsv"
BENCH_TABLE="runs.csv"

TIDYVCF_STACK="$RELEASE_BIN -i $TEST_VCF --stack --vep-fields -o $OUTPUT_TABLE"
TIDYVCF="$RELEASE_BIN -i $TEST_VCF --vep-fields -o $OUTPUT_TABLE"

if [[ ! -f runs.csv ]]
then
    echo "datetime,commit,command,mean,stddev,median,user,system,min,max" > $BENCH_TABLE
fi
 
mkfifo BENCHPIPE

hyperfine -w 3 --export-csv BENCHPIPE -- "${TIDYVCF} && ${TIDYVCF_STACK}" &

cat BENCHPIPE

echo -n "$(date -u),$(git rev-parse HEAD)," >> $BENCH_TABLE

cat BENCHPIPE | tail -n 1 >> $BENCH_TABLE &&

rm BENCHPIPE
rm $OUTPUT_TABLE
