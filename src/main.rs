use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    tidyvcf::tidyvcf()?;
    Ok(())
}
