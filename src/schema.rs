use noodles::vcf::header::Header;

use std::error::Error;

use crate::{
    consts::{DEFAULT_CSQ_OVERFLOW_FIELDNAME, DEFAULT_SAMPLE_STRING, MAIN_COLUMNS, VEP_FIELD_NAME},
    utils::split_header_csq_fields,
};

use crate::cli::Opt;

pub fn main_fields_strings(opt: &Opt) -> Vec<String> {
    let mut fields: Vec<String> = Vec::new();
    for col in MAIN_COLUMNS {
        if opt.include_column(col) {
            fields.push(col.to_owned());
        }
    }
    fields
}

pub fn info_fields_strings(
    fields: &mut Vec<String>,
    hdr: &Header,
    opt: &Opt,
) -> Result<usize, Box<dyn Error>> {
    let mut vep_fields_splitn = 0;
    for key in hdr.infos().keys() {
        if opt.vep_fields && key == VEP_FIELD_NAME {
            let csq_string = hdr
                .infos()
                .get(key)
                .ok_or("VEP field missing!")?
                .description();
            let mut csq_fields = split_header_csq_fields(csq_string);
            csq_fields.push(DEFAULT_CSQ_OVERFLOW_FIELDNAME.to_owned());
            vep_fields_splitn = csq_fields.len();
            for csq_field in csq_fields {
                fields.push(format!("{}{}", opt.vep_prefix, csq_field));
            }
        } else if opt.include_info_column(key.as_ref()) {
            fields.push(format!("{}{}", opt.info_prefix, key));
        }
    }
    Ok(vep_fields_splitn)
}

pub fn format_fields_strings(
    fields: &mut Vec<String>,
    hdr: &Header,
    opt: &Opt,
    samples: Vec<String>,
) {
    let mut added = Vec::new();

    for key in hdr.formats().keys() {
        if opt.include_column(key.as_ref()) {
            added.push(key.to_string());
        }
    }
    if opt.stack {
        added.push(DEFAULT_SAMPLE_STRING.to_owned());
        fields.append(&mut added);
    } else {
        for sample in samples {
            for field in &added {
                fields.push(format!("{}{}{}", &sample, &opt.format_delim, field));
            }
        }
    }
}
